package consts

const (
	CreateListQuery      = "INSERT INTO %s (title, descriptin) VALUES ($1, $2) RETURNING id"
	CreateUsersListQuery = "INSERT INTO %s (user_id, list_id) VALUES ($1, $2)"
	SelectTodoLists      = "SELECT tl.id, tl.title, tl.descriptin FROM %s tl INNER JOIN %s ul on tl.id = ul.list_id WHERE ul.user_id = $1"
	SelectTodoListById   = "SELECT tl.id, tl.title, tl.descriptin FROM %s tl INNER JOIN %s ul on tl.id = ul.list_id WHERE ul.user_id = $1 AND ul.list_id = $2"
	DeleteToDoListById   = "DELETE FROM %s tl USING %s ul WHERE tl.id = ul.list_id AND ul.user_id = $1 AND ul.list_id = $2"
	UpdateToDoListById   = "UPDATE %s tl SET %s FROM %s ul WHERE tl.id = ul.list_id AND ul.list_id=$%d AND ul.user_id = $%d"

	CreateUser = "INSERT INTO %s (name, username, password_hash) values ($1, $2, $3) RETURNING id"
	SelectUser = "SELECT * FROM %s WHERE username=$1 AND password_hash=$2"
)
