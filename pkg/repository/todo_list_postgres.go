package repository

import (
	"fmt"
	"rest"
	"rest/pkg/consts"
	"strings"

	"github.com/jmoiron/sqlx"
	"github.com/sirupsen/logrus"
)

type TodoListPostgres struct {
	db *sqlx.DB
}

func NewTodoListPostgres(db *sqlx.DB) *TodoListPostgres {
	return &TodoListPostgres{db: db}
}

func (r *TodoListPostgres) Create(userId int, list rest.TodoList) (int, error) {
	tx, err := r.db.Begin()
	if err != nil {
		return 0, err
	}

	var id int
	createListQuery := fmt.Sprintf(consts.CreateListQuery, todoListTable)
	row := tx.QueryRow(createListQuery, list.Title, list.Description)

	if err := row.Scan(&id); err != nil {
		tx.Rollback()
		return 0, err
	}

	createUsersListQuery := fmt.Sprintf(consts.CreateUsersListQuery, usersListsTable)
	_, err = tx.Exec(createUsersListQuery, userId, id)
	if err != nil {
		tx.Rollback()
		return 0, err
	}

	return id, tx.Commit()
}

func (r *TodoListPostgres) GetAll(userId int) ([]rest.TodoList, error) {
	var lists []rest.TodoList
	query := fmt.Sprintf(consts.SelectTodoLists, todoListTable, usersListsTable)
	err := r.db.Select(&lists, query, userId)

	return lists, err
}

func (r *TodoListPostgres) GetById(userId int, id int) (rest.TodoList, error) {
	var todoList rest.TodoList
	query := fmt.Sprintf(consts.SelectTodoListById, todoListTable, usersListsTable)
	err := r.db.Get(&todoList, query, userId, id)

	return todoList, err
}

func (r *TodoListPostgres) DeleteById(userId, id int) error {

	query := fmt.Sprintf(consts.DeleteToDoListById, todoListTable, usersListsTable)
	_, err := r.db.Exec(query, userId, id)

	return err
}

func (r *TodoListPostgres) Update(userId int, id int, input rest.UpdateListInput) error {
	setValues := make([]string, 0)
	args := make([]interface{}, 0)
	argId := 1

	if input.Title != nil {
		setValues = append(setValues, fmt.Sprintf("title=$%d", argId))
		args = append(args, *&input.Title)
		argId++
	}

	if input.Description != nil {
		setValues = append(setValues, fmt.Sprintf("descriptin=$%d", argId))
		args = append(args, *&input.Description)
		argId++
	}

	setQuery := strings.Join(setValues, ", ")

	query := fmt.Sprintf(consts.UpdateToDoListById, todoListTable, setQuery, usersListsTable, argId, argId+1)

	args = append(args, id, userId)

	logrus.Debugf("updateQuery: %s", query)
	logrus.Debugf("args: %s", args)

	_, err := r.db.Exec(query, args...)

	return err
}
