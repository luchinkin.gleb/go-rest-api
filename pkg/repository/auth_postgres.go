package repository

import (
	"fmt"
	"rest"
	"rest/pkg/consts"

	"github.com/jmoiron/sqlx"
)

type AuthPostgres struct {
	db *sqlx.DB
}

func NewAuthPostgres(db *sqlx.DB) *AuthPostgres {
	return &AuthPostgres{db: db}
}

func (r *AuthPostgres) CreateUser(user rest.User) (int, error) {
	var id int
	query := fmt.Sprintf(consts.CreateUser, userTable)
	row := r.db.QueryRow(query, user.Name, user.Username, user.Password)

	if err := row.Scan(&id); err != nil {
		return 0, err
	}

	return id, nil
}

func (r *AuthPostgres) GetUser(username, password string) (rest.User, error) {
	var user rest.User
	query := fmt.Sprintf(consts.SelectUser, userTable)
	err := r.db.Get(&user, query, username, password)

	fmt.Println(user, err)

	return user, err
}
