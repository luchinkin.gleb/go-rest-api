package service

import (
	"rest"
	"rest/pkg/repository"
)

type TodoListService struct {
	repo repository.TodoList
}

func NewTodoListService(repo repository.TodoList) *TodoListService {
	return &TodoListService{repo: repo}
}

func (s *TodoListService) Create(userId int, list rest.TodoList) (int, error) {
	return s.repo.Create(userId, list)
}

func (s *TodoListService) GetAll(userId int) ([]rest.TodoList, error) {
	return s.repo.GetAll(userId)
}

func (s *TodoListService) GetById(userId int, id int) (rest.TodoList, error) {
	return s.repo.GetById(userId, id)
}

func (s *TodoListService) DeleteById(userId, id int) error {
	return s.repo.DeleteById(userId, id)
}

func (s *TodoListService) Update(userId int, id int, input rest.UpdateListInput) error {
	if err := input.Validate(); err != nil {
		return err
	}
	return s.repo.Update(userId, id, input)
}
